﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinFromTest.View;

namespace WinFromTest
{
    public partial class Form1 : Form
    {
        private string[] TitleList = { "起始页", "设置语言", "身份验证", "用户是否存在", "指纹验证", "面部验证", "签入准时", "签入完成", "发送信息提醒", "完成"};

        private bool index = false;
        private int processStateIndex = 0;
        private Dictionary<Data.DataStructure.PageIndex, ContentView> Contents = new Dictionary<Data.DataStructure.PageIndex, ContentView>();

        private PageView_Start pageView_Start;
        private PageView_Language pageView_Language;
        private PageView_Recognize pageView_Recognize;
        private PageView_Logining pageView_Logining;
        private PageView_Finger pageView_Finger;
        private PageView_Facial pageView_Facial;
        private PageView_CheckinOnTime pageView_CheckinOnTime;
        private PageView_CheckInCompleted pageView_CheckInCompleted;
        private PageView_SendSmsRecord pageView_SendSmsRecord;
        private PageView_Exit pageView_Exit;

        public Form1()
        {
            InitializeComponent();
        }

        // 防止动态组件闪烁问题
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000; // Turn on WS_EX_COMPOSITED 
                return cp;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.pageView_Start = new PageView_Start();
            this.pageView_Start.Init(this.Content.Size, Data.DataStructure.PageIndex.Page_Start);
            this.Contents.Add(pageView_Start.id, pageView_Start);
            this.Content.Controls.Add(pageView_Start.panel);
            this.pageView_Start.ShowView(Data.DataStructure.PageIndex.Page_Start, this.TitleList[0]);

            this.pageView_Language = new PageView_Language();
            this.pageView_Language.Init(this.Content.Size, Data.DataStructure.PageIndex.Page_Language);
            this.Contents.Add(pageView_Language.id, pageView_Language);
            this.Content.Controls.Add(pageView_Language.panel);

            this.pageView_Recognize = new PageView_Recognize();
            this.pageView_Recognize.Init(this.Content.Size, Data.DataStructure.PageIndex.Page_Recongnize);
            this.Contents.Add(pageView_Recognize.id, pageView_Recognize);
            this.Content.Controls.Add(pageView_Recognize.panel);

            this.pageView_Logining = new PageView_Logining();
            this.pageView_Logining.Init(this.Content.Size, Data.DataStructure.PageIndex.Page_Existing);
            this.Contents.Add(pageView_Logining.id, pageView_Logining);
            this.Content.Controls.Add(pageView_Logining.panel);

            this.pageView_Finger = new PageView_Finger();
            this.pageView_Finger.Init(this.Content.Size, Data.DataStructure.PageIndex.Page_Finger);
            this.Contents.Add(pageView_Finger.id, pageView_Finger);
            this.Content.Controls.Add(pageView_Finger.panel);

            this.pageView_Facial = new PageView_Facial();
            this.pageView_Facial.Init(this.Content.Size, Data.DataStructure.PageIndex.Page_Facial);
            this.Contents.Add(pageView_Facial.id, pageView_Facial);
            this.Content.Controls.Add(pageView_Facial.panel);

            this.pageView_CheckinOnTime = new PageView_CheckinOnTime();
            this.pageView_CheckinOnTime.Init(this.Content.Size, Data.DataStructure.PageIndex.Page_CheckInOnTime);
            this.Contents.Add(pageView_CheckinOnTime.id, pageView_CheckinOnTime);
            this.Content.Controls.Add(pageView_CheckinOnTime.panel);

            this.pageView_CheckInCompleted = new PageView_CheckInCompleted();
            this.pageView_CheckInCompleted.Init(this.Content.Size, Data.DataStructure.PageIndex.Page_CheckCompleted);
            this.Contents.Add(pageView_CheckInCompleted.id, pageView_CheckInCompleted);
            this.Content.Controls.Add(pageView_CheckInCompleted.panel);

            this.pageView_SendSmsRecord = new PageView_SendSmsRecord();
            this.pageView_SendSmsRecord.Init(this.Content.Size, Data.DataStructure.PageIndex.Page_SendSMS_Remind);
            this.Contents.Add(pageView_SendSmsRecord.id, pageView_SendSmsRecord);
            this.Content.Controls.Add(pageView_SendSmsRecord.panel);

            this.pageView_Exit = new PageView_Exit();
            this.pageView_Exit.Init(this.Content.Size, Data.DataStructure.PageIndex.Page_Exit);
            this.Contents.Add(pageView_Exit.id, pageView_Exit);
            this.Content.Controls.Add(pageView_Exit.panel);
        }

        private void OnClickEnter(object sender, EventArgs e)
        {
            Console.WriteLine("進來了");
            var button = ((Button)sender);
            int posX = 0;
            if (index == false)
            {
                posX = button.Location.X - button.Size.Height - 100;
            }
            else
            {
                posX = button.Location.X + button.Size.Height + 100;
            }
            index = !index;
            ((Button)sender).Location = new Point(posX, button.Location.Y);
        }

        private void OnClickLeave(object sender, EventArgs e)
        {
            Console.WriteLine("出来了");
        }

        private void Dialog_Click(object sender, EventArgs e)
        {
            //弹出消息框，并获取消息框的返回值
            DialogResult dr = MessageBox.Show("是否打开新窗体？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            //如果消息框返回值是Yes，显示新窗体
            if (dr == DialogResult.Yes)
            {
                
            }
            //如果消息框返回值是No，关闭当前窗体
            else if (dr == DialogResult.No)
            {
                
            }
        }

        private void Next_Click(object sender, EventArgs e)
        {
            this.processStateIndex = ++this.processStateIndex % this.TitleList.Length;

            UpdateNetxOrAfterState();

            foreach (var content in this.Contents)
            {
                Data.DataStructure.PageIndex enumId = (Data.DataStructure.PageIndex)this.processStateIndex;
                content.Value.ShowView(enumId, this.TitleList[this.processStateIndex]);
            }

            var reqParams = new Dictionary<string, string>
            {
                ["roomId "] = "159"
            };

            NetWorkManager.Instance.RequestNetWork("http://www.icomi.cn:8000", "get", "/api/v2/pad/meeting/findTodayMeetingsByRoomId", reqParams, null, (result) => {
                Console.WriteLine(result);

            });

            Console.WriteLine("点击了下一步");
        }

        private void After_Click(object sender, EventArgs e)
        {
            if (this.processStateIndex == 0)    return;

            this.processStateIndex = --this.processStateIndex % this.TitleList.Length;

            UpdateNetxOrAfterState();

            foreach (var content in this.Contents)
            {
                Data.DataStructure.PageIndex enumId = (Data.DataStructure.PageIndex)this.processStateIndex;
                content.Value.ShowView(enumId, this.TitleList[this.processStateIndex]);
            }

            Console.WriteLine("点击了上一步");
        }

        private void UpdateNetxOrAfterState()
        {
            this.after.Visible = (this.processStateIndex != 0);
            if (this.processStateIndex == this.TitleList.Length - 1)
            {
                this.Next.Text = "完成";
            }
            else
            {
                this.Next.Text = "下一步";
            }
        }
    }
}
