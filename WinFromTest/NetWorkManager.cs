﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace WinFromTest
{
    class NetWorkManager
    {
        private static readonly NetWorkManager _instance = null;

        private string token = "android_faa720ea-18dd-419c-ba99-7f057be73044";

        static NetWorkManager()
        {
            _instance = new NetWorkManager();
        }
        private NetWorkManager()
        {

        }
        public static NetWorkManager Instance
        {
            get { return _instance; }
        }

        public void RequestNetWork(string url, string method, string action, Dictionary<string, string> arguments, string postData, Action<string> OnFinish)
        {
            if(action != null)
                url = url + action + "?token=" + token;
            if (arguments != null)
            {
                string param = "";
                foreach (KeyValuePair<string, string> item in arguments)
                {
                    var key = item.Key;
                    var val = item.Value;
                    param += "&" + key + "=" + val;
                }

                url += param;
            }

            if (method == "get")
            {
                
            }
            else if (method == "POST")
            {

            }

            WebRequest request = WebRequest.Create(url);
            request.Method = method;
            // If required by the server, set the credentials.
            request.Credentials = CredentialCache.DefaultCredentials;
            // Get the response.
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            // Display the status.
            Console.WriteLine(response.StatusDescription);
            // Get the stream containing content returned by the server.
            Stream dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            string responseFromServer = reader.ReadToEnd();

            OnFinish?.Invoke(responseFromServer);

            // Cleanup the streams and the response.
            reader.Close();
            dataStream.Close();
            response.Close();
        }
    }
}
