﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFromTest.Data
{
    public class DataStructure
    {
        public enum PageIndex {
            Page_Start = 0,                 // 起始页
            Page_Language,                  // 语言选择页
            Page_Recongnize,                // 身份验证页
            Page_Existing,                  // 存在的用户页
            Page_Finger,                    // 指纹验证页
            Page_Facial,                    // 人脸识别页
            Page_CheckInOnTime,             // 嵌入准时页
            Page_CheckCompleted,            // 登记完成页
            Page_SendSMS_Remind,            // 发送短信记录和提醒
            Page_Exit,                      // 完成页
            Page_Retry,                     // 超时页
            Page_SendSMS_Officer,           // 发短信通知站长
            Page_Prompt_Tocontact_Officer   // 请立即与站长联系页
        }
    }
}
