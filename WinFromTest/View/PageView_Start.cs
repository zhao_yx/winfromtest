﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFromTest.View
{
    class PageView_Start : ContentView
    {
        public override void AddModule()
        {
            Label b_start = new Label
            {
                Text = "签到系统",
                Size = new Size(100, 50),
                Location = new Point(size.Width / 2, size.Height / 2),
                Anchor = AnchorStyles.Right | AnchorStyles.Bottom
            };

            string str = Application.StartupPath;
            this.panel.BackgroundImage = new System.Drawing.Bitmap(str + "/Assets/startView.jpg");
        }
    }
}