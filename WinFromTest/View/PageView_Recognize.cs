﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFromTest.View
{
    class PageView_Recognize : ContentView
    {
        public override void AddModule()
        {
            Label userLable = new Label
            {
                Text = "用户名",
                TextAlign = ContentAlignment.MiddleRight,
                Location = new Point(244, 114),
                Size = new Size(120, 26),
                Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Bottom
            };


            TextBox user = new TextBox
            {
                Location = new Point(387, 114),
                Size = new Size(130, 26),
                Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Bottom
            };

            Label passwordLable = new Label
            {
                Text = "密码",
                TextAlign = ContentAlignment.MiddleRight,
                Location = new Point(244, 186),
                Size = new Size(120, 26),
                Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Bottom
            };


            TextBox password = new TextBox
            {
                PasswordChar = '*',
                Location = new Point(387, 186),
                Size = new Size(130, 26),
                Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Bottom
            };

            Button login = new Button
            {
                Text = "登录",
                Location = new Point(320, 250),
                Size = new Size(150, 26),
                Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Bottom,
            };

            login.MouseClick += new MouseEventHandler(OnClickLogin);

            this.panel.Controls.Add(userLable);
            this.panel.Controls.Add(user);
            this.panel.Controls.Add(passwordLable);
            this.panel.Controls.Add(password);
            this.panel.Controls.Add(login);
        }

        private void OnClickLogin(object sender, MouseEventArgs e)
        {
            Console.WriteLine("点击了注册");
        }

        /*public override void ShowView(Data.DataStructure.PageIndex showId, string title)
        {
            base.ShowView(showId, title);


        }*/
    }
}
