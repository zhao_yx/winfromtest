﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFromTest
{
    public abstract class ContentView
    {
        public Data.DataStructure.PageIndex id = 0;
        public Label title;
        public Panel panel;
        public Size size;

        public virtual void Init(Size size, Data.DataStructure.PageIndex id)
        {
            this.panel = new Panel
            {
                BackColor = Color.Yellow,
                Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Bottom,
                Size = new Size(size.Width, size.Height),
                BackgroundImageLayout = ImageLayout.Stretch
            };

            this.title = new Label
            {
                Size = new Size(size.Width, 50),
                BackColor = Color.Transparent,
                //AutoSize = true,
                Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top,
                TextAlign = ContentAlignment.MiddleCenter,
                Location = new Point(0, 5)
            };

            this.panel.Controls.Add(title);

            this.panel.Visible = false;

            this.id = id;
            this.size = size;

            this.AddModule();
        }

        public virtual void ShowView(Data.DataStructure.PageIndex showId, string title)
        {
            this.title.Text = title;
            this.panel.Visible = (showId == this.id);
        }

        public abstract void AddModule();
    }
}
