﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFromTest.View
{
    class PageView_Language : ContentView
    {
        public override void AddModule()
        {
            Button b_ch = new Button
            {
                Text = "繁体中文",
                Size = new Size(100, 50),
                Location = new Point(size.Width / 2 - 100, size.Height / 2),
                Anchor = AnchorStyles.Right | AnchorStyles.Bottom
            };

            Button b_en = new Button
            {
                Text = "Enlish",
                Size = new Size(100, 50),
                Location = new Point(size.Width / 2 + 100, size.Height / 2),
                Anchor = AnchorStyles.Right | AnchorStyles.Bottom
            };

            this.panel.Controls.Add(b_ch);
            this.panel.Controls.Add(b_en);
        }
    }
}
